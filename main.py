import re

placa = input("\n\tIntroduzca Numero de placa: ")

if re.match('(^[A-Z]{2})[0-9]{4}$', placa):
 print("\tEs una placa Particular")

elif re.match('^[0-9]{6}$', placa):
 print("\tEs una placa Particular")

elif re.match('^MB[0-9]{4}$', placa):
 print("\tEs una placa de Meterobus")

elif re.match('^F[0-9]{5}$', placa):
 print("\tEs una placa Fiscal")

elif re.match('^PR[0-9]{4}$', placa):
    print("\tEs una placa de Periodista")

elif re.match('^CP[0-9]{4}$', placa):
    print("\tEs una placa del Canal")

elif re.match('^D[0-9]{5}$', placa):
    print("\tEs una placa de Vehiculo de Demostracion")

elif re.match('^D[0-9]{5}$', placa):
    print("\tEs una placa de Cuerpo de Clausula")

elif re.match('^CC[0-9]{4}$', placa):
    print("\tEs una placa de Cuerpo Honorario")

elif re.match('^MI[0-9]{4}$', placa):
    print("\tEs una placa de Mision Diplomatica")

elif re.match('^CD[0-9]{4}$', placa):
    print("\tEs una placa de Cuerpo Diplomatico")

elif re.match('^M([A-Z]{1}||[0-9]){5}$', placa):
    print("\tEs una placa de Moto")

elif re.match('^T[0-9]{5}$', placa):
    print("\tEs una placa de Taxi")

elif re.match('^RI[0-9]{4}$', placa):
    print("\tEs una placa de Taxi de Ruta Interna")

else:
    print("\tNumero Invalido")
